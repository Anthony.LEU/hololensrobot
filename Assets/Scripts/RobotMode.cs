using MixedReality.Toolkit.SpatialManipulation;
using UnityEngine;

public class RobotMode : MonoBehaviour
{
    [Tooltip("Le parent du ARMeshManager qui cr�e les meshs")]
    public Transform _trackablesParent;

    public Transform _transformCamera;

    ObjectManipulator manipulator;
    BoundsControl control;

    GameObject boundingBox;
    GameObject meshs;

    void Start()
    {
        manipulator = GetComponent<ObjectManipulator>();
        control = GetComponent<BoundsControl>();
        ResetRobot();
    }

    void FindBoundingBox()
    {
        if (boundingBox == null)
        {
            boundingBox = transform.Find("BoundingBoxRobot(Clone)").gameObject;
        }
    }

    void FindMeshes()
    {
        if (meshs == null)
        {
            meshs = _trackablesParent.Find("Trackables").gameObject;
        }
    }

    public void ActivateMove()
    {
        manipulator.enabled = true;
        control.enabled = true;
        FindBoundingBox();
        if(boundingBox) boundingBox.SetActive(true);
    }

    public void DeactivateMove()
    {
        manipulator.enabled = false;
        control.enabled = false;
        FindBoundingBox();
        if (boundingBox) boundingBox.SetActive(false);
    }

    public void ResetRobot()
    {
        transform.position = _transformCamera.position + _transformCamera.forward * 3;
        transform.Translate(0, -transform.position.y, 0);
        transform.rotation = Quaternion.identity;
        transform.localScale = Vector3.one / 10;
    }
    
    public void EnableOcclusion()
    {
        FindMeshes();
        meshs.SetActive(true);
    }

    public void DisableOcclusion()
    {
        FindMeshes();
        meshs.SetActive(false);
    }
}
