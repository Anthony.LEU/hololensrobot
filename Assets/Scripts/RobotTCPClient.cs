using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using TMPro;
using System.Threading.Tasks;

public class RobotTCPClient : MonoBehaviour
{
    public TextMeshProUGUI _statusIcon;

    // IP et port du serveur
    private IPAddress serverIP;
    private int serverPort = 11235;

    // TCP client
    private TcpClient client;
    private NetworkStream stream;
    private byte[] receiveBuffer = new byte[24]; // 4 octets par float

    Color orange = new Color(255, 191, 0);

    private RobotControler _robotController;

    public TMP_InputField _serverIPField;

    void Start()
    {
        string serverIPString = PlayerPrefs.GetString("ip", "192.168.165.250");
        if (IPAddress.TryParse(serverIPString, out serverIP))
        {
            _serverIPField.text = serverIP.ToString();
        }
        InvokeRepeating("ConnectToServer", 0, 5); // Appelle la connexion toutes les cinq secondes
        _robotController = GetComponent<RobotControler>();
    }

    private async void ConnectToServer()
    {
        if (client != null) return;
        try
        {
            client = new TcpClient();
            //client.Connect(serverIP, serverPort);
            
            // passage de l'indicateur en orange
            _statusIcon.color = orange;

            // Connexion avec attente d'une seconde
            /*if(!client.ConnectAsync(serverIP, serverPort).Wait(1000))
            {
                // passage en rouge si �chec
                _statusIcon.color = Color.red;
                client = null;
                Debug.Log("D�lai de connexion au serveur d�pass�");
                return;
            }*/

            // Cr�e une t�che de connexion asynchrone et une t�che d'attente asynchrone d'une seconde
            var connectTask = client.ConnectAsync(serverIP, serverPort);
            var delayTask = Task.Delay(TimeSpan.FromSeconds(1));

            // on attend que l'une des deux soit compl�t�e
            var completedTask = await Task.WhenAny(connectTask, delayTask);

            // si la t�che compl�t�e est la t�che d'attente, il y a eu timeout
            if (completedTask == delayTask)
            {
                _statusIcon.color = Color.red;
                client = null;
                Debug.Log("D�lai de connexion au serveur d�pass�");
                return;
            }

            // si r�ussite, passage en vert
            _statusIcon.color = Color.green;

            // stockage de l'ip utilis�e pour la prochaine session
            PlayerPrefs.SetString("ip", serverIP.ToString());

            // Recuperation du flux
            stream = client.GetStream();

            // Recuperation asynchrone de donnees via un callback
            stream.BeginRead(receiveBuffer, 0, receiveBuffer.Length, ReceiveCallback, null);
        }
        catch (Exception e)
        {
            client?.Close();
            client = null;
            _statusIcon.color = Color.red;
            Debug.LogError($"Erreur de connexion au serveur : {e.Message}");
        }
    }

    // Callback de r�ception des donn�es
    private void ReceiveCallback(IAsyncResult result)
    {
        try
        {
            int bytesRead = stream.EndRead(result);
            if (bytesRead > 0)
            {
                // Conversion des octets en flottants
                float[] floats = new float[6];
                Buffer.BlockCopy(receiveBuffer, 0, floats, 0, receiveBuffer.Length);

                // Traitement de la donn�e re�ue (envoi au robot)
                _robotController.setNextJoint(floats);
                
                /*
                // Affichage de debug au cas o�
                string dataString = "Joints : ";
                for (int i = 0; i < floats.Length; i++)
                {
                    dataString += $"{floats[i]} | ";
                }
                Debug.Log(dataString);   
                */             

                // Continuer � recevoir la donn�e
                stream.BeginRead(receiveBuffer, 0, receiveBuffer.Length, ReceiveCallback, null);
            }
            else
            {
                // Connexion ferm�e
                _statusIcon.color = Color.red;
                client.Close();
                client = null;
                Debug.LogError("Connexion ferm�e par le serveur");
            }
        }
        catch (Exception e)
        {
            client?.Close();
            client = null;
            _statusIcon.color = Color.red;
            Debug.LogError($"Erreur lors de la r�ception des donn�es : {e.Message}");
        }
    }

    void OnDestroy()
    {
        // Fermeture du client � la destruction
        if (client != null)
        {
            client.Close();
        }
    }

    public void ChangeIPAddress(IPAddress ip)
    {
        serverIP = ip;
        if(client != null)
        {
            client.Close();
            client = null;
        }
        InvokeRepeating("ConnectToServer", 0, 5);
    }
}
