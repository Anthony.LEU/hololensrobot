using MixedReality.Toolkit.UX;
using UnityEngine;

public class DialogHandler : MonoBehaviour
{
    public DialogPool DialogPool;
    public RobotMode RobotMode;

    /// <summary>
    /// A Unity event function that is called when an enabled script instance is being loaded.
    /// </summary>
    protected virtual void Awake()
    {
        if (DialogPool == null)
        {
            DialogPool = GetComponent<DialogPool>();
        }
    }

    public void SpawnResetDialog()
    {
        IDialog dialog = DialogPool.Get()
            .SetHeader("Replacer")
            .SetBody("Voulez-vous vraiment replacer le robot devant vous ?")
            .SetPositive("Oui", (args) => RobotMode.ResetRobot())
            .SetNegative("Non");

        dialog.Show();
    }

    public void SpawnQuitDialog()
    {
        IDialog dialog = DialogPool.Get()
            .SetHeader("Quitter")
            .SetBody("Voulez-vous vraiment fermer l'application ?")
            .SetPositive("Oui", (args) => Application.Quit())
            .SetNegative("Non");

        dialog.Show();
    }
}
