using UnityEngine;

public class RobotControler : MonoBehaviour
{
    //[Range(-180, 180)]
    private float[] currJoints;
    //[Range(-180, 180)]
    private float[] nextJoints;
    //[Range(-180, 180)]
    private float[] stepJoints;
    //[Range(-180, 180)]
    private float[] initJoints = { 0f, 0f, 0f, 0f, 0f, 0f };
    void Start()
    {
        currJoints = new float[6];
        nextJoints = new float[6];
        stepJoints = new float[6];
        initJoints.CopyTo(currJoints,0);
        initJoints.CopyTo(nextJoints, 0);
        initJoints.CopyTo(stepJoints, 0);
    }

    public bool setNextJoint(float[] target) 
    { 
        target.CopyTo(nextJoints,0);
        return true;
    }

    public void AffectJoints()
    {
        Transform tf = transform.GetChild(0).GetChild(0).GetChild(0);
        tf.Rotate(new Vector3(0, 0, -stepJoints[0]));
        tf = tf.GetChild(0).GetChild(0);
        tf.Rotate(new Vector3(stepJoints[1], 0, 0));
        tf = tf.GetChild(0).GetChild(0); 
        tf.Rotate(new Vector3(stepJoints[2], 0, 0));
        tf = tf.GetChild(0).GetChild(0);
        tf.Rotate(new Vector3(0, stepJoints[3], 0));
        tf = tf.GetChild(0).GetChild(0).GetChild(0);
        tf.Rotate(new Vector3(stepJoints[4], 0, 0));
        tf = tf.GetChild(0);
        tf.Rotate(new Vector3(0, 0, stepJoints[5]));
    }
    public void goToNextJoint()
    {
        for (int i = 0; i < 6; i++)
        {
            stepJoints[i] = (nextJoints[i] - currJoints[i]);
            currJoints[i] += stepJoints[i];
        }
        AffectJoints();
    }

    void Update()
    {
        if (!currJoints.Equals(nextJoints))
        {
            goToNextJoint();
        }
    }
}
