using MixedReality.Toolkit;
using MixedReality.Toolkit.UX;
using System.Net;
using TMPro;
using UnityEngine;

public class NumpadHandler : MonoBehaviour
{
    // les boutons du numpad
    public StatefulInteractable _bouton1, _bouton2, _bouton3, _bouton4,
        _bouton5, _bouton6, _bouton7, _bouton8, _bouton9, _bouton0,
        _boutonPoint, _boutonValider, _boutonDel, _boutonClear;
    
    // le champ de texte pour l'ip serveur
    public TMP_InputField _inputFieldText;

    // le script client tcp
    public RobotTCPClient _clientTCP;

    // Permet l'envoi de la boite de dialogue si l'ip est invalide
    public DialogPool DialogPool;

    private void Awake()
    {
        _bouton1.OnClicked.AddListener(() => _inputFieldText.text += '1');
        _bouton2.OnClicked.AddListener(() => _inputFieldText.text += '2');
        _bouton3.OnClicked.AddListener(() => _inputFieldText.text += '3');
        _bouton4.OnClicked.AddListener(() => _inputFieldText.text += '4');
        _bouton5.OnClicked.AddListener(() => _inputFieldText.text += '5');
        _bouton6.OnClicked.AddListener(() => _inputFieldText.text += '6');
        _bouton7.OnClicked.AddListener(() => _inputFieldText.text += '7');
        _bouton8.OnClicked.AddListener(() => _inputFieldText.text += '8');
        _bouton9.OnClicked.AddListener(() => _inputFieldText.text += '9');
        _bouton0.OnClicked.AddListener(() => _inputFieldText.text += '0');
        _boutonPoint.OnClicked.AddListener(() => _inputFieldText.text += '.');
        _boutonDel.OnClicked.AddListener(() => _inputFieldText.text = _inputFieldText.text.Remove(_inputFieldText.text.Length - 1));
        _boutonClear.OnClicked.AddListener(() => _inputFieldText.text = "");
        _boutonValider.OnClicked.AddListener(() => TryChangeIPAddress());
    }

    // Start is called before the first frame update
    void Start()
    {
        DialogPool = GetComponent<DialogPool>();
    }

    void TryChangeIPAddress()
    {
        IPAddress ip;
        if(IPAddress.TryParse(_inputFieldText.text, out ip))
        {
            _clientTCP.ChangeIPAddress(ip);
            _inputFieldText.DeactivateInputField();
            gameObject.SetActive(false);
        }
        else
        {
            IDialog dialog = DialogPool.Get()
            .SetHeader("IP invalide")
            .SetBody("L'IP que vous avez rentr�e n'est pas valide.")
            .SetNeutral("Ok");

            dialog.Show();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
